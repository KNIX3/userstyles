# Iceberg Theme for Invidious
Not all Invidious instances are supported, but most are.

See the `@-moz-document` line and feel free to add other instances as they will most likely work.
## Dark Mode
![A screenshot of the theme in dark mode](screenshots/dark-mode.png)
## Light Mode
![A screenshot of the theme in light mode](screenshots/light-mode.png)
